package com.eureka.FoodAppEureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FoodAppEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodAppEurekaApplication.class, args);
	}

}
