package com.payment.foodApp.models;

import lombok.*;

@Data @Getter @Setter @ToString @AllArgsConstructor @NoArgsConstructor
public class PaymentRequest {

    private String amount;
    private Card cardDetails;
    private String toMerchant;

}
