package com.payment.foodApp.models;

import lombok.*;

@Data
@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
public class Card {

    private String debitCardNo;
    private String expiryDate;
    private int cvv;
    private String nameOnCard;
}
