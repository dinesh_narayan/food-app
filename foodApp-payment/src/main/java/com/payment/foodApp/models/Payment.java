package com.payment.foodApp.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data @Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString
@Document(collection = "payments")
public class Payment {

    @Id
    private String id;
    private String amount;
    private String user;
    private String merchant;
    private statusEnum status;

}
