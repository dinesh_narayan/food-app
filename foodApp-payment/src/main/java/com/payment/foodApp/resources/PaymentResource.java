package com.payment.foodApp.resources;

import com.google.gson.Gson;
import com.netflix.discovery.converters.Auto;
import com.payment.foodApp.models.Payment;
import com.payment.foodApp.models.PaymentRequest;
import com.payment.foodApp.models.statusEnum;
import com.payment.foodApp.repo.PaymentRepo;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/payments")
public class PaymentResource {

    @Autowired
    PaymentRepo repo;
    @Autowired
    MongoTemplate template;
    @Autowired
    RabbitTemplate rabbitTemplate;
    Payment payment = new Payment();

    //Do Payment
    // 0.Save the payment with pending status
    // 1.Send Payment Message to Rabbit queue
    // 2.Save the response from payment listener service and update in ID

    //MakePayment - I~Amount,Card Details,To Merchant
    //PaymentRequest
    @PostMapping(path = "/do")
    public String makePayment(@RequestBody PaymentRequest request) {

         payment = Arrays.asList(request).stream().map((requests)->{

            payment.setAmount(request.getAmount());
            payment.setMerchant(request.getToMerchant());
            payment.setStatus(statusEnum.PENDING);
            payment.setUser(request.getCardDetails().getNameOnCard());

            return payment;
        }).collect(Collectors.toList()).get(0);
        repo.save(payment);

        rabbitTemplate.convertAndSend("payment-exchange","foodAppPayment",payment);
        return "sent";
    }
    @PostMapping(path="/donePayment", produces = MediaType.APPLICATION_JSON_VALUE)
    public Payment updatePayment(@RequestBody String paymentSt) {

        Payment payment = new Gson().fromJson(paymentSt,Payment.class);
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(payment.getId()));
        Update update = new Update();
        update.set("status",statusEnum.SUCCESS);
        template.findAndModify(query,update,Payment.class);
        return payment;

    }

}
