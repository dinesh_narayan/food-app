package com.zuul.FoodAppZuul;

import com.zuul.FoodAppZuul.models.UserModel;
import com.zuul.FoodAppZuul.repo.UserModelRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
public class FoodAppZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodAppZuulApplication.class, args);
	}

	//@Bean
	public CommandLineRunner createUsers(UserModelRepo repo) {
		return strings->{
			Arrays.asList(new UserModel("Dinesh", "$2y$12$tFiVjbtkQRY850uwuwX87u90VszfwWOJeqysGa0XZ9zmRHRziqqnC", true, new String[]{"ROLE_ADMIN","ROLE_USER"})).stream().forEach((user)->{repo.save(user);});
		};
	}

}
