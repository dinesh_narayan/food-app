package com.zuul.FoodAppZuul.models;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "foodapp_users")
@Data @ToString @Getter@Setter
public class UserModel {

    @Id
    private String id;
    private String userName;
    private String password;
    private boolean isActive;
    private String[] roles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public UserModel() {
    }

    public UserModel(String userName, String password, boolean isActive, String[] roles) {
        this.userName = userName;
        this.password = password;
        this.isActive = isActive;
        this.roles = roles;
    }
}
