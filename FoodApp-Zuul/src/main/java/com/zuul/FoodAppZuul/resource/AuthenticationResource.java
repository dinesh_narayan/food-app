package com.zuul.FoodAppZuul.resource;

import com.zuul.FoodAppZuul.config.JWTUtil;
import com.zuul.FoodAppZuul.models.AuthenticationRequest;
import com.zuul.FoodAppZuul.models.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class AuthenticationResource {

    @Autowired
    JWTUtil jwtUtil;
    @Autowired
    UserDetailsService service;
    @Autowired
    AuthenticationManager manager;

    @GetMapping(path = "/authenticate" , produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthenticationResponse createJwt(@RequestBody AuthenticationRequest request) {

        UsernamePasswordAuthenticationToken tk = new UsernamePasswordAuthenticationToken(request.getUserName(),request.getPassword());
        manager.authenticate(tk);
        UserDetails details = service.loadUserByUsername(request.getUserName());
        String jwt = jwtUtil.generateToken(details);
        return new AuthenticationResponse(jwt);

    }

}
