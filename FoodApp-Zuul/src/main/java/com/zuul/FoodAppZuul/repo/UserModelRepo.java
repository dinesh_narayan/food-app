package com.zuul.FoodAppZuul.repo;

import com.zuul.FoodAppZuul.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserModelRepo extends MongoRepository<UserModel,String> {

    Optional<UserModel> findByUserName(String userName);

}
