package com.zuul.FoodAppZuul.config;

import com.zuul.FoodAppZuul.models.UserModel;
import com.zuul.FoodAppZuul.repo.UserModelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserModelRepo repo;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Optional<UserModel> user = repo.findByUserName(s);
        user.orElseThrow(()->new UsernameNotFoundException("User ID does not Exist"));
        UserDetails userDetails = Arrays.asList(user.get()).stream().map(UserDetailsImpl::new).collect(Collectors.toList()).get(0);
        return userDetails;
    }
}
