package com.orders.FoodAppOrder.services;

import com.orders.FoodAppOrder.models.Order;
import com.orders.FoodAppOrder.models.OrderRequest;
import com.orders.FoodAppOrder.models.statusEnum;
import com.orders.FoodAppOrder.repo.OrderRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepo repo;

    @Override
    public Order submitOrder(OrderRequest request) {
        log.info("Enters SubmitOrder");
        Order order = new Order();
        order.setOrderStatus(statusEnum.PENDING);
        order.setOrderTime(new Timestamp(System.currentTimeMillis()));
        order.setRequest(request);
        repo.save(order);
        return order;
    }

    @Override
    public Order viewOrder(String id) throws Exception {
        log.info("Enters viewOrder");
        return repo.findById(id).orElseThrow(()->new Exception("No Such User"));
    }

    @Override
    public String deleteOrder(String id) {
        log.info("Delete Order");
        repo.deleteById(id);
        return "Deleted Successfully";
    }

    @Override
    public List<Order> viewOrdersByDate(Date date) {
        log.info("View Orders By Date");
        List<Order> orderTime = repo.findByOrderTimeGreaterThan(date, Sort.by(Sort.Direction.ASC,"orderTime"));
        return orderTime;

    }
}
