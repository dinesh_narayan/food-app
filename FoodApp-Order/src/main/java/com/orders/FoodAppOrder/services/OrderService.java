package com.orders.FoodAppOrder.services;

import com.orders.FoodAppOrder.models.Order;
import com.orders.FoodAppOrder.models.OrderRequest;

import java.util.Date;
import java.util.List;

public interface OrderService {
    Order submitOrder(OrderRequest request);

    Order viewOrder(String id) throws Exception;

    String deleteOrder(String id);

    List<Order> viewOrdersByDate(Date date);
}
