package com.orders.FoodAppOrder.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Timestamp;
import java.util.Date;

@Data
@Getter@Setter
@Document(collection = "orders")
public class Order {

    @Id
    private String orderID;
    private Date orderTime;
    private OrderRequest request;
    private statusEnum orderStatus;

}
