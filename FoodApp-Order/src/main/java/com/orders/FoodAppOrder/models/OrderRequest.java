package com.orders.FoodAppOrder.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString
public class OrderRequest {

    private String restID;
    private String restName;
    private String userID;
    private String userName;
    private String itemName;

    public OrderRequest() {
    }

    public String getRestID() {
        return restID;
    }

    public OrderRequest(String restID, String restName, String userID, String userName, String itemName) {
        this.restID = restID;
        this.restName = restName;
        this.userID = userID;
        this.userName = userName;
        this.itemName = itemName;
    }
}
