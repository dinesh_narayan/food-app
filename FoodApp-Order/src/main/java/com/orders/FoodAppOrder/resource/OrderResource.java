package com.orders.FoodAppOrder.resource;

import com.orders.FoodAppOrder.models.Order;
import com.orders.FoodAppOrder.models.OrderRequest;
import com.orders.FoodAppOrder.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderResource {
    @Autowired
    private OrderService orderService;

    /*----------------------------------*/
    //#Submit A Order/ Inputs~ Restaurant - ID,Name,Item, UserDetail - userID, userName/ Output~Order Details
    /*----------------------------------*/
    @PostMapping(path = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public Order submitOrders(@RequestBody OrderRequest request) {
        return orderService.submitOrder(request);
    }

    /*----------------------------------*/
    //#View A Order / Inputs~Order ID / Output~Order Details
    /*----------------------------------*/
    @GetMapping(path = "/view/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Order viewOrder(@PathVariable("id") String id) throws Exception {
        return orderService.viewOrder(id);
    }

    /*----------------------------------*/
    //Delete A Order //Inputs~OrderID //Success
    /*----------------------------------*/
    @DeleteMapping(path="/delete/{id}")
    public String deleteOrder(@PathVariable("id") String id) {
        return orderService.deleteOrder(id);
    }

    /*----------------------------------*/
    //Group Orders By Date /Inputs~Date/userID /Outputs~List of Orders
    /*----------------------------------*/
    @GetMapping(path = "/viewByDate/{date}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> viewOrdersByDate(@PathVariable("date") String date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date dtDate = dateFormat.parse(date);
        return orderService.viewOrdersByDate(dtDate);
    }



}
