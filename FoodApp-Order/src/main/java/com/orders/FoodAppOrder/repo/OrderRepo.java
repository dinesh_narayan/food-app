package com.orders.FoodAppOrder.repo;

import com.orders.FoodAppOrder.models.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepo extends MongoRepository<Order,String>, MongoCustomRepo {

    List<Order> findByOrderTimeGreaterThan(Date date, Sort sort);

}
