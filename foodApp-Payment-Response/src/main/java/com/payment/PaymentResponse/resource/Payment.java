package com.payment.foodApp.models;

import com.payment.PaymentResponse.resource.statusEnum;
import lombok.*;


@Data @Getter @Setter @AllArgsConstructor @NoArgsConstructor @ToString
public class Payment {


    private String id;
    private String amount;
    private String user;
    private String merchant;
    private statusEnum status;

}
