package com.payment.PaymentResponse.resource;

import com.payment.foodApp.models.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
public class PaymentResponseResource {

    @Autowired
    RestTemplate template;

    @RabbitListener(queues = "payment-request-queue")
    public void getPaymentResponse(String message) {
        log.info("Message From Queue {}",message);
        template.postForObject("http://payment-service/payments/donePayment",message, String.class);
    }

}

