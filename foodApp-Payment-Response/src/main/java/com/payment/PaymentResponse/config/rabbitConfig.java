package com.payment.PaymentResponse.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class rabbitConfig {

    //Exchange
    public TopicExchange getExchange() {
        return new TopicExchange("payment-exchange");
    }

    //Define Queues
    //@Bean
    public Queue getQueue() {
        return new Queue("payment-request-queue");
    }

    // Bind Queues with Routing Key
    //@Bean
    public Binding bindQueuesRequest() {
        return BindingBuilder.bind(getQueue()).to(getExchange()).with("foodAppPayment");
    }

    //Defing Rabbit Template Bean
    //@Bean
    public AmqpTemplate getRabbitTemplate(ConnectionFactory factory) {
        final RabbitTemplate template = new RabbitTemplate();
        template.setMessageConverter(jacksonConverter());
        return template;
    }

    //@Bean
    public MessageConverter jacksonConverter() {

        return new Jackson2JsonMessageConverter();

    }

}
