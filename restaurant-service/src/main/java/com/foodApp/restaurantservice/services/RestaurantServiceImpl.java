package com.foodApp.restaurantservice.services;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Rating;
import com.foodApp.restaurantservice.model.Restaurant;
import com.foodApp.restaurantservice.repo.RestaurantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    RestaurantRepo repo;



    @Override
    public List<Restaurant> getAll() {

        return repo.findAllByOrderByRestaurantNameAsc();
    }

    @Override
    public Restaurant getOneByID(String id) {
        return repo.findById(id).orElse(null);
    }

    @Override
    public List<Item> getItemsByRestaurantID(String id) {

        return repo.getItemsFromRestaurantID(id);

    }

    @Override
    public boolean addRatingForRestaurant(String id, Rating rating) {

        Restaurant rest = repo.findById(id).get();
        repo.addRatingForRestaurant(rest, id, rating);

        rating.setRestID(id);

        return true;


    }
}
