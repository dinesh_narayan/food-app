package com.foodApp.restaurantservice.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Getter@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Document(collection = "ratings")
public class Rating {

    @Id @JsonProperty("Rating ID")
    private String id;
    private String ratingDesc;
    private String rating;
    private String restID;

    public String getRating() {
        return rating;
    }

    public Rating() {
    }

    @JsonCreator
    public Rating(@JsonProperty("Rating Description") String ratingDesc, @JsonProperty("Rating") String rating, @JsonProperty("Restaurant ID") String restID) {
        this.ratingDesc = ratingDesc;
        this.rating = rating;
        this.restID = restID;
    }

    public void setRestID(String restID) {
        this.restID = restID;
    }
}
