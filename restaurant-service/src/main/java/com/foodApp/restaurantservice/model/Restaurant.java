package com.foodApp.restaurantservice.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@Getter@Setter
@Document(collection = "restaurant")
public class Restaurant{

    @Id
    @JsonProperty("Restaurant ID")
    private String id;
    private String restaurantName;
    private List<Item> items;
    @DBRef
    //@CascadeSave
    private List<Rating> ratings;
    private double averageRating;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonCreator
    public Restaurant(@JsonProperty("Restaurant Name") String restaurantName, @JsonProperty("Restaurant Items") List<Item> items/*, @JsonProperty("Restaurant Ratings") List<Rating> ratings*/) {
        this.restaurantName = restaurantName;
        this.items = items;
        //this.ratings = ratings;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }
}
