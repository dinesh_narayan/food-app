package com.foodApp.restaurantservice.repo;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Rating;
import com.foodApp.restaurantservice.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MongoCustomDBImpl implements MongoCustomDB {

    @Autowired
    MongoTemplate mTemplate;


    double sum = 0;

    @Override
    public List<Item> getItemsFromRestaurantID(String id) {
        System.out.println("For Commit");
        Query query = new Query();
        query.addCriteria(Criteria
        .where("_id").is(id));
         Restaurant rest = mTemplate.findOne(query, Restaurant.class);
         return rest.getItems().stream().sorted(
                 Comparator.comparing(Item::getItemName)
        ).collect(Collectors.toList());

    }

    @Override
    public boolean addRatingForRestaurant(Restaurant rest, String id, Rating rating) {

       /* Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Restaurant rest = mTemplate.findOne(query,Restaurant.class);*/





        if(rest.getRatings()!=null)
            rest.getRatings().add(rating);
        else {
            List<Rating> ratingNew = new ArrayList<>();
            rest.setRatings(ratingNew);
            rest.getRatings().add(rating);
        }
        rest.setRatings(rest.getRatings().stream().filter((ratingOp)->ratingOp.getRating()!=null).collect(Collectors.toList()));
        rest.getRatings().stream().forEach((eachRating)-> {
            sum = sum + Double.parseDouble(eachRating.getRating());
        });
        double average = sum/rest.getRatings().size();
        rest.setAverageRating(average);
        Update update = new Update();
        update.set("ratings",rest.getRatings());
        DecimalFormat format = new DecimalFormat("#.#");
        update.set("averageRating",format.format(average));
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        mTemplate.findAndModify(query, update,Restaurant.class);
        return true;
    }


}
