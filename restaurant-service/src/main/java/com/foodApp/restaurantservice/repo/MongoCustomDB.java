package com.foodApp.restaurantservice.repo;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Rating;
import com.foodApp.restaurantservice.model.Restaurant;

import java.util.List;

public interface MongoCustomDB {

    List<Item> getItemsFromRestaurantID(String id);

    boolean addRatingForRestaurant(Restaurant rest, String id, Rating rating);
}
